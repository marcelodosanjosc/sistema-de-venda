package inter.bootcamp.service;

import org.springframework.stereotype.Service;
import org.w3c.dom.ls.LSOutput;

import java.util.Objects;

@Service
public class GreetingService {

    public String sayHi() {
        return this.sayHi("Mundo");
    }

    public String sayHi(String name) {
        return "Ola " + Objects.requireNonNullElse(name, "Mundo");
    }

    ///////////////////////////////////////////////////////////////////////////
    // aqui ficar um comentario
    ///////////////////////////////////////////////////////////////////////////

    public String nome = "Nome";
}
